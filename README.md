# scss-bootstrap-overrides

stembord bootstrap overrides

```json
"dependencies": {
  "@stembord/bootstrap": "git://gitlab.com/stembord/scss-bootstrap-overrides.git"
}
```

```scss
@import "../node_modules/@stembord/bootstrap-overrides/scss/index.scss";
@import "../node_modules/bootstrap/scss/bootstrap.scss";
```
